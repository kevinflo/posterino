if (process.env.NODE_ENV === "production" || typeof jest !== 'undefined') {
  module.exports = require("./configureStore.prod");
} else {
  module.exports = require("./configureStore.dev");
}

