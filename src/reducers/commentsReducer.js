import * as types from "../actions/actionTypes";

export default (state = {}, action) => {
  let payload = action.payload;
  switch (action.type) {
    case types.COMMENTS_REQUEST:
      return {
        ...state,
        [payload.postId]: {
          pending: true
        }
      };
    case types.COMMENTS_SUCCESS:
      return {
        ...state,
        [payload.postId]: {
          allComments: payload.allComments
        }
      };
    case types.COMMENTS_FAILURE:
      return {
        ...state,
        [payload.postId]: {
          error: payload.error
        }
      };
    default:
      return state;
  }
};
