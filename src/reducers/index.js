import postsReducer from "./postsReducer";
import commentsReducer from "./commentsReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  posts: postsReducer,
  comments: commentsReducer
});

export default rootReducer;
