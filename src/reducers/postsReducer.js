import * as types from "../actions/actionTypes";

export default (state = {}, action) => {
  let payload = action.payload;
  switch (action.type) {
    case types.POSTS_REQUEST:
      return {
        pending: true
      };
    case types.POSTS_SUCCESS:
      return {
        allPosts: payload.posts,
        userPosts: payload.userPosts
      };
    case types.POSTS_FAILURE:
      return {
        error: payload.error
      };
    default:
      return state;
  }
};
