import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as postActions from "../actions/postActions";
import PostsList from "../components/PostsList";
import LoadingSpinner from "../components/LoadingSpinner";

const styles = {
  userPageHeader: {
    marginBottom: "40px"
  },
  error: {
    color: "red"
  }
};

class PostsPage extends Component {
  componentDidMount() {
    this.props.actions.loadPosts();
  }

  render() {
    const { posts, pending, error, userId } = this.props;
    if (error) {
      return (
        <span style={styles.error}>
          An error occurred retrieving the posts. Please try again later.
        </span>
      );
    }

    return (
      <div>
        {userId && (
          <h1 className="text-center" style={styles.userPageHeader}>
            Posts from User {userId}
          </h1>
        )}
        {!pending && posts ? <PostsList posts={posts} /> : <LoadingSpinner />}
      </div>
    );
  }
}

let dressPosts = (postsState, userId) => {
  let { allPosts, userPosts } = postsState;
  let dressedPosts = [];
  let postIds;

  if (userId && typeof userPosts === "object") {
    postIds = userPosts[userId] || [];
  } else {
    postIds = typeof allPosts === "object" ? Object.keys(allPosts) : [];
  }

  postIds.forEach(postId => {
    dressedPosts.push(allPosts[postId]);
  });

  return dressedPosts;
};

const mapStateToProps = (state, ownProps) => {
  let userId = ownProps.match.params.userId;
  let postId = ownProps.match.params.postId;
  let pending = state.posts.pending;
  let posts = dressPosts(state.posts, userId);

  return {
    posts,
    pending,
    userId,
    postId
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(postActions, dispatch)
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PostsPage)
);
