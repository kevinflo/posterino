import React from "react";
import { Provider } from "react-redux";
import DevTools from "./DevTools";
import { Route } from "react-router-dom";
import PostsPage from "./PostsPage";
import PostModal from "./PostModal";
import { Grid, Row, Col } from "react-bootstrap";
import AppNavbar from "../components/AppNavbar";

const Root = ({ store }) => {
  return (
    <Provider store={store}>
      <div>
        <AppNavbar />
        <Grid id="app-holder">
          <Row>
            <Col xs={12}>
              <Route path="*" component={PostModal} />
              <Route exact path="/" component={PostsPage} />
              <Route path="/users/:userId" component={PostsPage} />
              <Route path="/users/:userId/posts/:postId" component={PostsPage} />
              <Route path="/posts/:postId" component={PostsPage} />
              <DevTools />
            </Col>
          </Row>
        </Grid>
      </div>
    </Provider>
  );
};

export default Root;
