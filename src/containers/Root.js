if (process.env.NODE_ENV === "production" || typeof jest !== "undefined") {
  module.exports = require("./Root.prod");
} else {
  module.exports = require("./Root.dev");
}