import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import * as commentActions from "../actions/commentActions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PostModalContent from "../components/PostModalContent";

const extractEntityIdFromPath = (entityType, path) => {
  let postId;

  let splitPath = path.split(`/${entityType}s/`);
  let pathPostIdComponent = splitPath[splitPath.length - 1];
  if (pathPostIdComponent) {
    postId = pathPostIdComponent.split("/")[0];
  }

  return postId;
};

class PostModal extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    let userId = this.props.userId;
    let modalDismissalPath = userId ? `/users/${userId}` : "/";
    this.props.history.push(modalDismissalPath);
  }

  componentDidUpdate(prevProps) {
    let postId = this.props.postId;

    if (!postId || postId === prevProps.postId) {
      return;
    }

    this.props.actions.loadComments(postId);
  }

  componentDidMount() {
    let postId = this.props.postId;

    if (!postId) {
      return;
    }

    this.props.actions.loadComments(postId);
  }

  render() {
    const { post, comments, pending, postId, error } = this.props;

    return (
      <Modal show={!!postId} onHide={this.handleClose}>
        <PostModalContent
          post={post}
          comments={comments}
          error={error}
          pending={pending}
          postId={postId}
          handleClose={this.handleClose}
        />
      </Modal>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let userId = extractEntityIdFromPath("user", ownProps.match.url);
  let postId = extractEntityIdFromPath("post", ownProps.match.url);

  let post = {};
  let comments;
  let pending;
  let error;

  if (postId) {
    post =
      (state.posts && state.posts.allPosts && state.posts.allPosts[postId]) ||
      {};
    let commentsState = state.comments[postId];
    comments = commentsState && commentsState.allComments;
    pending = commentsState && commentsState.pending;
    error = commentsState && commentsState.error;
  }

  return {
    comments,
    pending,
    postId,
    post,
    userId,
    error
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(commentActions, dispatch)
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(PostModal)
);
