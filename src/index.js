import React from "react";
import ReactDOM from "react-dom";
import 'bootstrap/dist/css/bootstrap.css';
import "./App.css";
import configureStore from "./store/configureStore";
import { BrowserRouter as Router } from "react-router-dom";
import Root from "./containers/Root";

const store = configureStore();
ReactDOM.render(
  <Router>
    <Root store={store} />
  </Router>,
  document.getElementById("root")
);
