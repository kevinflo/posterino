import React from "react";
import { ListGroup } from "react-bootstrap";
import Comment from "./Comment";

const PostsList = props => {
  const { comments } = props;

  return (
    <ListGroup>
      {comments &&
        comments.map(comment => (
          <Comment key={comment.id} comment={comment} />
        ))}
    </ListGroup>
  );
};

export default PostsList;
