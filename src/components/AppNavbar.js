import React from "react";
import { Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";

const styles = {
  link: {
    color: "#0084B4"
  }
};

export default () => {
  return (
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <Link style={styles.link} to="/">
            Posterino
          </Link>
        </Navbar.Brand>
      </Navbar.Header>
    </Navbar>
  );
};
