import React from "react";
import { ListGroupItem, Row, Col, Glyphicon } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";

const styles = {
  row: {
    marginBottom: "1rem"
  },
  vcenter: {
    float: "none",
    display: "inline-block",
    verticalAlign: "middle"
  },
  hcenter: {
    marginLeft: "auto",
    marginRight: "auto"
  },
  userContent: {
    borderRadius: "50%",
    backgroundColor: "white"
  },
  glyphiconUser: {
    fontSize: "4rem"
  }
};

const Post = props => {
  const { post, onUserRoute, match } = props;
  let userId = match.params.userId;

  let postPath = onUserRoute
    ? `/users/${post.userId}/posts/${post.id}`
    : `/posts/${post.id}`;

  return userId ? (
    <Row style={styles.row}>
      <Col xs={12}>
        <LinkContainer to={postPath}>
          <ListGroupItem>
            <h3>{post.title}</h3>
            <p>{post.body}</p>
          </ListGroupItem>
        </LinkContainer>
      </Col>
    </Row>
  ) : (
    <Row style={styles.row}>
      <Col
        xs={3}
        md={2}
        className="text-center"
        style={{ ...styles.vcenter, ...styles.hcenter }}
      >
        <div>
          <Link to={`/users/${post.userId}`}>
            <Glyphicon glyph="user" style={styles.glyphiconUser} />
            <br />
            User {post.userId}
          </Link>
        </div>
      </Col>
      <Col xs={9} md={10} style={styles.vcenter}>
        <LinkContainer to={postPath}>
          <ListGroupItem>
            <h3>{post.title}</h3>
            <p>{post.body}</p>
          </ListGroupItem>
        </LinkContainer>
      </Col>
    </Row>
  );
};

export default withRouter(Post);
