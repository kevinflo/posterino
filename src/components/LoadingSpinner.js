import React from "react";
import spinner from "../spinner.svg";
import { Row, Col } from "react-bootstrap";

const LoadingSpinner = () => (
  <Row>
    <Col xs={12} className="text-center">
      <img src={spinner} alt="Loading..." />
    </Col>
  </Row>
);

export default LoadingSpinner;
