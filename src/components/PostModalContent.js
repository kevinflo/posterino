import React, { Component, Fragment } from "react";
import CommentsList from "./CommentsList";
import LoadingSpinner from "./LoadingSpinner";
import { Modal, Button } from "react-bootstrap";

const styles = {
  error: {
    color: "red"
  }
};

class PostModalContent extends Component {
  shouldComponentUpdate(nextProps) {
    return !!(nextProps && nextProps.postId);
  }

  render() {
    const { post, comments, pending, error, handleClose } = this.props;

    return (
      <Fragment>
        <Modal.Header closeButton>
          <Modal.Title>{post.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>{post.body}</p>
          <h3 className="text-center">
            {comments && comments.length} Comments:
          </h3>
          {error && (
            <span style={styles.error}>
              An error occurred retrieving the comments. Please try again later.
            </span>
          )}
          {pending ? <LoadingSpinner /> : <CommentsList comments={comments} />}
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleClose}>Close</Button>
        </Modal.Footer>
      </Fragment>
    );
  }
}

export default PostModalContent;
