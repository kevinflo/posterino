import React from "react";
import { ListGroupItem } from "react-bootstrap";

const styles = {
  p: {
    color: "grey",
    marginTop: ".5rem"
  }
};

const Post = props => {
  const { comment } = props;
  return (
    <ListGroupItem>
      <h4>{comment.name}:</h4> {comment.body}
      {comment.email && <p style={styles.p}>From: {comment.email}</p>}
    </ListGroupItem>
  );
};

export default Post;
