import React from "react";
import { ListGroup } from "react-bootstrap";
import Post from "./Post";
import { withRouter } from "react-router-dom";

const PostsList = props => {
  const { posts, match } = props;

  return (
    <ListGroup>
      {posts.map(post => (
        <Post post={post} key={post.id} onUserRoute={!!match.params.userId} />
      ))}
    </ListGroup>
  );
};

export default withRouter(PostsList);
