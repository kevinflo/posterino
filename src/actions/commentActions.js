import * as types from "./actionTypes";
import axios from "axios";
import { apiBase } from "../constants";

let shouldFetchComments = (comments, postId) => {
  return (
    !comments ||
    !comments[postId] ||
    (!comments[postId].allComments && !comments[postId].pending)
  );
}

let normalizeComments = comments =>{
  if (!Array.isArray(comments)) {
    return [];
  }

  return comments;
}

export function loadComments(postId) {
  return async function(dispatch, getState) {
    let commentsState = getState().comments;

    /*
      This conditional short circuts everything so we only ever fetch comments once.
      For a more long-lived or real world application we wouldn't do this as we'd need
      to paginate or otherwise fetch more comments over time.
    */
    if (!shouldFetchComments(commentsState, postId)) {
      return;
    }

    dispatch(commentsRequest(postId));

    axios
      .get(`${apiBase}/${postId}/comments`)
      .then(response => {
        if (!response || !response.data) {
          dispatch(commentsFailure(postId, "An error has occurred"));
        } else {
          let comments = normalizeComments(response.data);
          
          dispatch(commentsSuccess(postId, comments));
        }
      })
      .catch(function(error) {
        dispatch(commentsFailure(postId, error));
      });
  };
}

const commentsRequest = postId => {
  return {
    type: types.COMMENTS_REQUEST,
    payload: {
      postId
    }
  };
};

const commentsSuccess = (postId, allComments) => {
  return {
    type: types.COMMENTS_SUCCESS,
    payload: {
      postId,
      allComments
    }
  };
};
const commentsFailure = (postId, error) => {
  return {
    type: types.COMMENTS_FAILURE,
    payload: {
      postId,
      error
    }
  };
};
