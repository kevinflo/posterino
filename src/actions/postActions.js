import * as types from "./actionTypes";
import axios from "axios";
import { apiBase } from "../constants";

let shouldFetchPosts = posts => {
  const { allPosts } = posts;
  return (
    !posts ||
    (posts &&
      (posts.pending ||
        typeof allPosts !== "object" ||
        Object.keys(allPosts).length === 0))
  );
};

let normalizePosts = posts => {
  if (!Array.isArray(posts)) {
    return {};
  }

  return posts.reduce(
    (prev, curr) => {
      const { userId, title, body, id } = curr;

      if (!userId || (!title && !body)) {
        return prev;
      }

      prev["allPosts"][id] = curr;

      if (!prev["userPosts"][userId]) {
        prev["userPosts"][userId] = [id];
      } else {
        prev["userPosts"][userId].push(id);
      }

      return prev;
    },
    { userPosts: {}, allPosts: {} }
  );
};

export function loadPosts() {
  return async function(dispatch, getState) {
    let postsState = getState().posts;

    /*
      This conditional short circuts everything so we only ever fetch posts once.
      For a more long-lived or real world application we wouldn't do this as we'd need
      to paginate or otherwise fetch more posts over time.
    */
    if (!shouldFetchPosts(postsState)) {
      return;
    }

    dispatch(postsRequest());

    axios
      .get(apiBase)
      .then(response => {
        if (!response || !response.data) {
          dispatch(postsFailure("An error has occurred"));
        } else {
          let normalizedPosts = normalizePosts(response.data);

          dispatch(
            postsSuccess(normalizedPosts.allPosts, normalizedPosts.userPosts)
          );
        }
      })
      .catch(function(error) {
        dispatch(postsFailure(error));
      });
  };
}

const postsRequest = () => {
  return {
    type: types.POSTS_REQUEST
  };
};

const postsSuccess = (posts, userPosts) => {
  return {
    type: types.POSTS_SUCCESS,
    payload: {
      posts,
      userPosts
    }
  };
};
const postsFailure = error => {
  return {
    type: types.POSTS_FAILURE,
    payload: {
      error
    }
  };
};
